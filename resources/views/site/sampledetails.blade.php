@extends("layout.site.master")
@section('content')
    @include("layout.site.blocks.internalpage")
    <br>
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <ul>
                    <li class="homebreadcrumbs">
                        <a href="index.php">خانه</a> <span class="flaticon flaticon-left-arrow"></span>
                    </li>
                    <li>
                        <a href="sample.php">نمونه کار</a> <span class="flaticon flaticon-left-arrow"></span>
                    </li>
                    <li>
                        <a href="sample.php">سایت ستارک</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
@endsection