@extends('layout.admin.master')
@section('content')

    <form method="POST" action="{{url('/admin/product/add')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <label>نام :</label>
        <input class="form-control" type="text" name="title">
        <p class="help-block">حروف نباید بیشتر از ۲۵۵ تا باشد</p>
        <br>
        <label>توضیحات :</label>
        <input class="form-control" type="text" name="description">
        <br>
        <label>عکس محصول :</label>
        <input type="file" name="image">
        <br>
        <label><input value="1" type="checkbox" name="status">وضعیت</label>
        <br>
        <br>
        <input type="submit" value="ارسال">
    </form>

@endsection