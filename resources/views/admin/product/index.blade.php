@extends('layout.admin.master')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">لیست محصولات</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
                                            <th>شماره</th>
                                            <th>عنوان</th>
                                            <th>عکس محصولات</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($product as $row)
                                        <tr class="gradeA odd" role="row">
                                            <td class="sorting_1">{{$row->id}}</td>
                                            <td class="sorting_1">{{$row->title}}</td>
                                            <td class="sorting_1"><img style="width:100px;height:100px" src="{{asset('assets/uploads/uploader/medium/'.$row->image)}}" alt=""></td>
                                            <td class="sorting_1">@if($row->status) فعال@else غیرفعال@endif</td>
                                            <td class="sorting_1"><a href="{{url('admin/product/delete/'.$row->id)}}">delete</a> <a href="{{url('admin/product/edit/'.$row->id)}}">edit</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection