@extends('layout.admin.master')
@section('content')

    <form method="POST" action="{{url('/admin/product/add')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <label>نام :</label>
        <input class="form-control" type="text" name="title" value="{{$product->title}}">
        <input class="form-control" type="hidden" name="id" value="{{$product->id}}">
        <p class="help-block">حروف نباید بیشتر از ۲۵۵ تا باشد</p>
        <br>
        <label>توضیحات :</label>
        <textarea class="form-control" name="description" rows="3">{{$product->description}}</textarea>
        <br>
        <label>عکس محصول :</label>
        <td><img src="{{asset('assets/uploads/uploader/medium'.$product->image)}}" alt=""></td>
        <input type="file" name="image" value="{{$product->image}}">
        <br>
        <label><input value="1" type="checkbox" name="status" @if($product->status) checked @endif>وضعیت</label>
        <br>
        <br>
        <input type="submit" value="ذخیره">
    </form>

@endsection