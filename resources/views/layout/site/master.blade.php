<!DOCTYPE html>
<html lang="fa">
    @include('layout.site.blocks.head')
	<body class="footerBG">
        @include('layout.site.blocks.menu')
        
            @yield('content')
    
        @include('layout.site.blocks.footer')
        @include('layout.site.blocks.script')
	</body>
</html>