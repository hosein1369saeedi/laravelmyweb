<section id="box" class="sample-e">
    <h2 class="alphabet credwh text-center h2"><img src="{{asset('assets/site/images/logo/interior design(3).png')}}" alt="" style="width:3%"> نمونه کار</h2>
    <img src="{{asset('assets/site/images/qwqw.png')}}" alt="" style="width:20%">
    <hr>
    <br>
    <div class="row">
        <!--box-1-->
        <div class="col-md-4">
            <div class="box-1 wow  wow bounceInRight" data-wow-duration="5s" >
                <div class="box20 blue">
                    <img src="{{asset('assets/site/images/sample/sample-1.png')}}" alt="">
                    <div class="box-content">
                        <h3 class="title text-right">سایت آموزشی ' ستارک '</h3>
                        <p>
                            <a class="button" href="{{url('/sampledetails')}}">مشاهده جزییات</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
            <!--box-2-->
        <div class="col-md-4">
            <div class="box-1 wow wow bounceInLeft" data-wow-duration="3s" >
                <div class="box20 blue">
                    <img src="{{asset('assets/site/images/sample/sample-2.png')}}" alt="">
                    <div class="box-content">
                        <h3 class="title text-right">سایت شرکتی ' پارس ترونیک '</h3>
                        <p>
                            <a class="button" href="{{url('/sampledetails')}}">مشاهده جزییات</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
            <!--box-3-->
        <div class="col-md-4">
            <div class="box-1 wow bounceInUp center" data-wow-duration="1s" data-wow-delay="0s">
                <div class="box20 blue">
                    <img src="{{asset('assets/site/images/sample/sample-3.png')}}" alt="">
                    <div class="box-content">
                        <h3 class="title text-right">سایت فروشگاهی ' گلدن۸ '</h3>
                        <p>
                            <a class="button" href="{{url('/sampledetails')}}">مشاهده جزییات</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <a href="{{url('/samplelist')}}" class="sample-a">مشاهده بیشتر...</a>
    </div>
</section>