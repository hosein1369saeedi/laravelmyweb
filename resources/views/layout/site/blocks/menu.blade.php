<section class="menu">
    <div id="myNav" class="overlay">
        <a href="index.html" style="margin:0.5%;margin-left: 2%;">
            <span class="flaticon flaticon-scientist" style="color:#fff;font-size: 3vw"></span>
        </a>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
            <span class="flaticon flaticon-cancel"></span>
        </a>
        <div class="overlay-content">
            <a href="{{url('/')}}">خانه</a>
            <a href="{{url('/gallery')}}">گالری</a>
            <a href="{{url('/samplelist')}}">نمونه کار</a>
            <a href="{{url('/contact')}}">تماس با ما</a>
            <a href="{{url('/about')}}">درباره ما</a>
        </div>
    </div>
    <span class="iconnav"onclick="openNav()">
        <span class="flaticon flaticon-menu"></span>
    </span>
    <div class="login">
        <ul>
            <li>
                <a href="{{url('/')}}">
                    <span class="flaticon flaticon-scientist"></span>
                </a>
            </li>
            <li>
                <button type="button" class="transparent btn" data-toggle="modal" data-target="#exampleModalRegister">
                    <span class="flaticon flaticon-keyhole"></span>
                </button>
            </li>
            <li>
                <button type="button" class="transparent btn" data-toggle="modal" data-target="#exampleModalUser">
                    <span class="flaticon flaticon-user"></span>
                </button>
            </li>
        </ul>
    </div>
</section>

<!-- Modal User -->
<div class="modal fade" id="exampleModalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Register -->
<div class="modal fade" id="exampleModalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>