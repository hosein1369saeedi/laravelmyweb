<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="{{asset('assets/site/images/logo/interior design(2).png')}}">

    <link href="{{asset('assets/site/css/bootstrap.min.css')}}" rel="stylesheet" crossorigin="anonymous">
    <link href="{{asset('assets/site/css/bootstrap-grid.min.css')}}" rel="stylesheet" crossorigin="anonymous">
    <link href="{{asset('assets/site/css/animate.css')}}" rel="stylesheet" crossorigin="anonymous">
    <link href="{{asset('assets/site/fonts/myicons/flaticon.css')}}" rel="stylesheet">
    <link href="{{asset('assets/site/css/style.css')}}" rel="stylesheet" crossorigin="anonymous">
</head>