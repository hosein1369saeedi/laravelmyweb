<section class="slider">
    <div id="slider-animation" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{asset('assets/site/images/slider/slide-5.jpg')}}" alt="Chicago">
                <a href="#">
                    <div class="text-box">
                        <div class="row align-items-center">
                            <div class="col-md-6  wow slideInRight hhh" data-wow-duration="4s">
                                <h2 class="credwh text-right alphabet h2">
                                    <img src="{{asset('assets/site/images/logo/interior design(2).png')}}" alt="" style="width:7%"> 
                                    وب سایت
                                </h2>
                                <p class="cgoldwh text-right digital p">طراحی وب سایت های فروشگاهی , شرکتی , آموزشگاهی و . . .</p>
                            </div>
                            <div class="col-md-6  wow slideInLeft" data-wow-duration="4s">
                                <img src="{{asset('assets/site/images/png/png-2.png')}}">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="carousel-item">
                <img src="{{asset('assets/site/images/slider/slide-2.jpg')}}" alt="Los Angeles">
                <a href="">
                    <div class="text-box">
                        <div class="row align-items-center">
                            <div class="col-md-6 hhh">
                                <h2 class="wow fadeInUp credwh text-right alphabet h2" data-wow-duration="4s">
                                    <img src="{{asset('assets/site/images/logo/interior design(2).png')}}" alt="" style="width:7%"> 
                                    اپللیکیشن
                                </h2>
                                <p class="wow fadeInUp  cgoldwh text-right digital p" data-wow-duration="2s">طراحی اپللیکیشن های فروشگاهی , شرکتی , آموزشگاهی و . . .</p>
                            </div>
                            <div class="col-md-6 wow slideInLeft" data-wow-duration="4s">
                                <img src="{{asset('assets/site/images/png/app.png')}}">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- <div class="carousel-item">
                <img src="{{asset('assets/site/images/slider/slide-1.jpg')}}" alt="Los Angeles">
                <div class="text-box col-md-6 hhh">
                    <h2 class="wow slideInRight credwh text-right alphabet h2" data-wow-duration="2s">
                        <img src="{{asset('assets/site/images/logo/interior design(2).png')}}" alt="" style="width:5%"> 
                        مشاوره و بهینه سازی سئو
                    </h2>
                    <p class="wow slideInLeft cgoldwh text-right digital p" data-wow-duration="2s">طراحی اپللیکیشن های فروشگاهی , شرکتی , شخصی و . . .</p>
                </div>
            </div> -->
        </div>
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#slider-animation" data-slide="prev">
            <span class="flaticon flaticon-left-arrow" style="font-size:3vw;color: #ff5c5c"></span>
        </a>
        <a class="carousel-control-next" href="#slider-animation" data-slide="next">
            <span class="flaticon flaticon-right-arrow" style="font-size:3vw;color: #ff5c5c"></span>
        </a>
    </div>
</section>