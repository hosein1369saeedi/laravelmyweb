<!DOCTYPE html>
<html lang="en">
    @include('layout.admin.blocks.head')
    <body>
        <div id="wrapper">
            @include('layout.admin.blocks.nav')
            <div id="page-wrapper">
                @yield('content')
            </div>
        </div>
        @include('layout.admin.blocks.script')
    </body>
</html>
