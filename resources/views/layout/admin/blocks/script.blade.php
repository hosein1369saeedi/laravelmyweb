<!-- jQuery Version 1.11.0 -->
<script src="{{asset('assets/admin/js/jquery-1.11.0.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{asset('assets/admin/js/metisMenu/metisMenu.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{asset('assets/admin/js/raphael/raphael.min.js')}}"></script>
<script src="{{asset('assets/admin/js/morris/morris.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{asset('assets/admin/js/sb-admin-2.js')}}"></script>