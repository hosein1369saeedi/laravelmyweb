<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/admin','Admin\HomeController@getIndex');
Route::get('admin/product/list','Admin\ProductController@getIndex');
Route::get('admin/product/add','Admin\ProductController@getAddproduct');
Route::post('admin/product/add','Admin\ProductController@postAddProduct');
Route::get('admin/product/delete/{id}','Admin\ProductController@getDelete');
Route::get('admin/product/edit/{id}','Admin\ProductController@getEditProduct');
Route::post('admin/product/edit','Admin\ProductController@postEditProduct');
Route::get('/', function () {
    return view('site.index');
});
Route::get('/contact', function () {
    return view('site.contact');
});
Route::get('/about', function () {
    return view('site.about');
});
Route::get('/sampledetails', function () {
    return view('site.sampledetails');
});
Route::get('/samplelist', function () {
    return view('site.samplelist');
});
Route::get('/gallery', function () {
    return view('site.gallery');
});