<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getIndex(Request $request) {
        $product = Product::select()->count();
        return view('admin.dashboard')->with('product',$product);
    }
}
