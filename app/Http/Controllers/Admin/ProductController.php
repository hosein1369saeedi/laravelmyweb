<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Classes\Resizer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function getIndex(Request $request) {
        $query = Product::select()->get();
        return view('admin.product.index')->with('product',$query);
    }
    public function getAddproduct() {
        return view('admin.product.add');
    }
    public function postAddProduct(ProductRequest $request) {
        $input = $request->all();
            if ($request->hasFile('image')) {
                $pathMain = "assets/uploads/uploader/";
                $pathMedium = "assets/uploads/uploader/medium/";
                $pathBig = "assets/uploads/uploader/big/";
                $extension = $request->file('image')->getClientOriginalExtension();
                $ext = ['jpeg','jpg','png'];
            if (in_array($extension, $ext)) {
                $fileName = md5(microtime()) . ".$extension";
                $request->file('image')->move($pathMain, $fileName);
                Resizer::resizePic($pathMain . $fileName, $pathMedium . $fileName, 1349, 600, $extension);
                Resizer::resizePic($pathMain . $fileName, $pathBig . $fileName, 1000, 800, $extension, True);
                $input['image'] = $fileName;
            } else {
                return Product::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }
        }
        $input['status'] = $request->has('status');
        $product = Product::create($input);
        return redirect('admin/product/list');
    }
    public function getDelete($id) {
        $pathMedium = "assets/uploads/uploader/medium/";
        $pathBig = "assets/uploads/uploader/big/";
        $product = product::find($id);
        File::delete($pathMedium . $product->image);
        File::delete($pathBig . $product->image);
        $product->delete();
        return redirect('admin/product/list');
    }
    public function getEditProduct($id) {
        $list = product::find($id);
        return view('admin.product.edit')
        ->with('product',$list);
    }
    public function postEditProduct(ProductRequest $request) {
        $input = $request->all();
        $product = Product::find($input['id']);
            if ($request->hasFile('image')) {
                $pathMain = "assets/uploads/uploader/";
                $pathMedium = "assets/uploads/uploader/medium/";
                $pathBig = "assets/uploads/uploader/big/";
                File::delete($pathMedium . $product->image);
                File::delete($pathBig . $product->image);
                $extension = $request->file('image')->getClientOriginalExtension();
                $ext = ['jpeg','jpg','png'];
            if (in_array($extension, $ext)) {
                $fileName = md5(microtime()) . ".$extension";
                $request->file('image')->move($pathMain, $fileName);
                Resizer::resizePic($pathMain . $fileName, $pathMedium . $fileName, 1349, 600, $extension);
                Resizer::resizePic($pathMain . $fileName, $pathBig . $fileName, 1000, 800, $extension, True);
                $input['image'] = $fileName;
            } else {
                return Product::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }
        }
        $input['status'] = $request->has('status');
        $product->update($input);
        return redirect('admin/product/list');
    }
}
